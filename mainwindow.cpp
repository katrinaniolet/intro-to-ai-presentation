#include <QFileDialog>
#include <QDebug>
#include "mainwindow.h"
#include "hopfieldnetwork.h"
#include "matrixdatadialog.h"
#include "ui_mainwindow.h"

static const int DEFAULT_SIZE = 8;

MainWindow::MainWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MainWindow),
    m_network(new HopfieldNetwork(DEFAULT_SIZE*DEFAULT_SIZE,this)),
    m_matrixDataDialog(new MatrixDataDialog(m_network,this))
{
    ui->setupUi(this);
    ui->grid->setSize(DEFAULT_SIZE);

    connect(ui->clearButton,SIGNAL(clicked()),ui->grid,SLOT(clear()));
    connect(ui->resetButton,SIGNAL(clicked()),this,SLOT(reset()));
    connect(ui->recallButton,SIGNAL(clicked()),this,SLOT(recall()));
    connect(ui->trainButton,SIGNAL(clicked()),this,SLOT(train()));
    connect(ui->showPreset,SIGNAL(clicked()),this,SLOT(showPreset()));
    connect(ui->loadImage,SIGNAL(clicked()),this,SLOT(loadImage()));
    connect(ui->showGrid,SIGNAL(toggled(bool)),this,SLOT(toggleGrid(bool)));
    connect(ui->grid,SIGNAL(sizeChanged(int)),this,SLOT(resetFinished()));
    connect(ui->grid,SIGNAL(sizeAboutToChange(int)),this,SLOT(resetStarted(int)));

    m_matrixDataDialog->setAttribute(Qt::WA_DeleteOnClose);
    m_matrixDataDialog->show();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::train()
{
    setCursor(Qt::WaitCursor);
    Q_ASSERT(m_network);
    m_network->train(ui->grid->toVector());
    setCursor(Qt::ArrowCursor);
}

void MainWindow::recall()
{
    setCursor(Qt::WaitCursor);
    Q_ASSERT(m_network);
    ui->grid->fromVector(m_network->recall(ui->grid->toVector()));
    setCursor(Qt::ArrowCursor);
}

void MainWindow::reset()
{
    Q_ASSERT(m_network);
    int size = ui->gridSize->value();
    m_network->clearAndResize(size*size);
    QMetaObject::invokeMethod(ui->grid,"setSize", Qt::QueuedConnection, Q_ARG(int,ui->gridSize->value()) );
}

void MainWindow::resetStarted(int size)
{
    setCursor(Qt::WaitCursor);
    if(size > 16) {
        ui->grid->showGrid(false);
        ui->showGrid->setChecked(false);
    }
    else {
        ui->grid->showGrid(true);
        ui->showGrid->setChecked(true);
    }
    ui->grid->setVisible(false);
}

void MainWindow::resetFinished()
{
    ui->grid->setVisible(true);
    setCursor(Qt::ArrowCursor);
}

void MainWindow::showPreset()
{
    Q_ASSERT(m_network);
    ui->grid->fromPredefined(ui->preset->currentIndex());
}

void MainWindow::loadImage()
{
    setCursor(Qt::WaitCursor);
    QString file = QFileDialog::getOpenFileName(this,"Select Image");
    QImage image(file);
    int size = ui->grid->size();
    image = image.scaled(QSize(size,size),Qt::IgnoreAspectRatio);
    image = image.convertToFormat(QImage::Format_MonoLSB);
    QList<int> data;
    for(int i=0;i<size;++i) {
        for(int j=0;j<size;++j) {
            if(image.pixel(j,i) == 4278190080)
                data.push_back(1);
            else
                data.push_back(-1);
        }
    }
    ui->grid->fromVector(data);
    setCursor(Qt::ArrowCursor);
}

void MainWindow::toggleGrid(bool show)
{
    ui->grid->showGrid(show);
}

