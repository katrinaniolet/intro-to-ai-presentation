#include <QtGlobal>
#include "hopfieldnetwork.h"
#include "matrix.h"

HopfieldNetwork::HopfieldNetwork(int size, QObject *parent) :
    QObject(parent),
    m_matrix(Matrix(size,size))
{
}

HopfieldNetwork::~HopfieldNetwork()
{
}

Matrix HopfieldNetwork::matrix() const
{
    return m_matrix;
}

int HopfieldNetwork::size() const
{
    Q_ASSERT(m_matrix.rowCount() == m_matrix.columnCount());
    return m_matrix.rowCount();
}

void HopfieldNetwork::clearAndResize(int size)
{
    emit dataAboutToChange();
    m_matrix.clearAndResize(size,size);
    emit dataChanged();
}

QList<int> HopfieldNetwork::recall(const QList<int> &input)
{
    Q_ASSERT(m_matrix.columnCount() == m_matrix.rowCount());
    Q_ASSERT(input.size() == m_matrix.rowCount());

    QList<int> out;
    Matrix inputMatrix = Matrix::rowMatrixFromList(input);
    for(std::size_t column=0; column < input.size(); ++column ) {
        Matrix columnMatrix = m_matrix.columnAsMatrix(column);

        int result = dotProduct(inputMatrix,columnMatrix);
        if(result>0)
            out.push_back(1);
        else
            out.push_back(-1);
    }
    return out;
}

void HopfieldNetwork::train(const QList<int> &input)
{
    Q_ASSERT(m_matrix.columnCount() == m_matrix.rowCount());
    Q_ASSERT(input.size() == m_matrix.rowCount());

    emit dataAboutToChange();

    Matrix inputMatrix = Matrix::rowMatrixFromList(input);
    Matrix dataMatrix = inputMatrix.transposed() * inputMatrix;
    Matrix identity = Matrix::identityMatrix(dataMatrix.rowCount());
    Matrix final = dataMatrix-identity;
    m_matrix = m_matrix + final;
    emit dataChanged();
}
