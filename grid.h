#ifndef GRID_H
#define GRID_H

#include <QFrame>
class QGridLayout;
class GridCell;

class Grid : public QFrame
{
    Q_OBJECT
    Q_PROPERTY(int size READ size WRITE setSize NOTIFY sizeChanged)

public:
    enum PreDefined {
        A8 = 0,
        F8,
        O8,
        Equals8,
        Box8,
        Checker8
    };

    explicit Grid(QWidget *parent = 0);

    int size() const;
    QList<int> toVector() const;

signals:
    void sizeAboutToChange( int );
    void sizeChanged( int );

public slots:
    void setSize( int size );
    void clear();
    void fromVector( const QList<int> &input );
    void fromPredefined( int item );
    void showGrid( bool show );

private:
    int m_size;
    bool m_showGrid;
    QList< GridCell* > m_cells;
    QGridLayout * m_layout;
};

#endif // GRID_H
