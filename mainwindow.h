#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDialog>
class HopfieldNetwork;
class MatrixDataDialog;

namespace Ui {
class MainWindow;
}

class MainWindow : public QDialog
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void train();
    void recall();
    void reset();
    void resetStarted( int size );
    void resetFinished();
    void showPreset();
    void loadImage();
    void toggleGrid(bool);

private:
    Ui::MainWindow *ui;
    HopfieldNetwork *m_network;
    MatrixDataDialog *m_matrixDataDialog;
};

#endif // MAINWINDOW_H
