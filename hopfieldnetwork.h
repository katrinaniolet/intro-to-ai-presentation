#ifndef HOPFIELDNETWORK_H
#define HOPFIELDNETWORK_H

#include <QObject>
#include <vector>
#include "matrix.h"

class HopfieldNetwork : public QObject
{
    Q_OBJECT
public:
    HopfieldNetwork( int size = 8, QObject * parent = 0 );
    ~HopfieldNetwork();
    Matrix matrix() const;
    int size() const;
    QList<int> recall( const QList<int> & input );

signals:
    void dataAboutToChange();
    void dataChanged();

public slots:
    void clearAndResize( int size );
    void train( const QList<int> & input );

private:
    Matrix m_matrix;
};

#endif // HOPFIELDNETWORK_H
