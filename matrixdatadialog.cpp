#include "matrixdatadialog.h"
#include "matrixtablemodel.h"
#include "ui_matrixdatadialog.h"

MatrixDataDialog::MatrixDataDialog(HopfieldNetwork * network, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MatrixDataDialog)
{
    ui->setupUi(this);
    MatrixTableModel * model = new MatrixTableModel(network,ui->tableView);
    connect(model,SIGNAL(layoutChanged()),this,SLOT(resizeColumns()),Qt::QueuedConnection);
    ui->tableView->setModel(model);
    QMetaObject::invokeMethod(this,"resizeColumns",Qt::QueuedConnection);
    QFont font(ui->tableView->font());
    font.setPointSize(7);
    ui->tableView->setFont(font);
    ui->tableView->verticalHeader()->setFont(font);
    ui->tableView->horizontalHeader()->setFont(font);
}

MatrixDataDialog::~MatrixDataDialog()
{
    delete ui;
    ui = 0;
}

void MatrixDataDialog::resizeColumns()
{
    if(ui && ui->tableView) {
        ui->tableView->resizeColumnsToContents();
        ui->tableView->resizeRowsToContents();
    }
}
