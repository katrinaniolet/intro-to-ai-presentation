#ifndef MATRIX_H
#define MATRIX_H

#include <QSharedData>
#include <QSharedDataPointer>
#include <QList>

class MatrixData : public QSharedData
{
public:

    MatrixData();
    MatrixData( const MatrixData & other );
    virtual ~MatrixData();

    int** m_data;
    std::size_t m_columns;
    std::size_t m_rows;
};

class Matrix
{
public:
    Matrix();
    Matrix( std::size_t rows, std::size_t columns );
    Matrix( const Matrix & other );

    virtual ~Matrix();

    static Matrix identityMatrix( std::size_t size );
    static Matrix rowMatrixFromList( const QList<int> &list );
    static Matrix columnMatrixFromList( const QList<int> & list );

    std::size_t rowCount() const;
    std::size_t columnCount() const;
    int value( std::size_t row, std::size_t column ) const;
    void setValue( std::size_t row, std::size_t column, int value);
    bool isVector() const;

    Matrix rowAsMatrix( std::size_t row ) const;
    Matrix columnAsMatrix( std::size_t column ) const;
    Matrix transposed() const;

    void clearAndResize( std::size_t rows, std::size_t colmuns );

    Matrix operator+( const Matrix & other );
    Matrix operator-( const Matrix & other );
    Matrix operator*( const Matrix & other );

private:
    QSharedDataPointer<MatrixData> d;
};

int dotProduct( Matrix mat1, Matrix mat2 );

#endif // MATRIX_H
