#include <QtGlobal>
#include "matrix.h"

Matrix::Matrix() :
    d(new MatrixData())
{
}

Matrix::Matrix(std::size_t rows, std::size_t columns) :
    d(new MatrixData())
{
    clearAndResize(rows,columns);
}

Matrix::Matrix(const Matrix &other) :
    d(other.d)
{
}

Matrix::~Matrix()
{
}

Matrix Matrix::identityMatrix(std::size_t size)
{
    Matrix newMatrix(size, size);
    for(std::size_t i = 0; i < size; ++i){
        newMatrix.setValue(i, i, 1);
    }
    return newMatrix;
}

Matrix Matrix::rowMatrixFromList(const QList<int> &list)
{
    Matrix newMatrix(1, list.size());
    for(std::size_t i = 0; i < list.size(); ++i){
        newMatrix.setValue(0, i, list.at(i));
    }
    return newMatrix;
}

Matrix Matrix::columnMatrixFromList(const QList<int> &list)
{
    Matrix newMatrix(list.size(), 1);
    for(std::size_t i = 0; i < list.size(); ++i){
        newMatrix.setValue(i, 0, list.at(i));
    }
    return newMatrix;
}

std::size_t Matrix::rowCount() const
{
    return d->m_rows;
}

std::size_t Matrix::columnCount() const
{
    return d->m_columns;
}

int Matrix::value(std::size_t row, std::size_t column) const
{
    Q_ASSERT(d->m_data);
    Q_ASSERT(row < rowCount());
    Q_ASSERT(column < columnCount());
    return d->m_data[row][column];
}

void Matrix::setValue(std::size_t row, std::size_t column, int value)
{
    Q_ASSERT(row < rowCount());
    Q_ASSERT(column < columnCount());
    d->m_data[row][column] = value;
}

bool Matrix::isVector() const
{
    return (rowCount() == 1 || columnCount() == 1);
}

Matrix Matrix::rowAsMatrix(std::size_t row) const
{
    Q_ASSERT(row < rowCount());
    Matrix newMatrix(1, d->m_columns);
    for (std::size_t i = 0; i < d->m_columns; ++i){
        newMatrix.setValue(0, i, value(row, i));
    }
    return newMatrix;
}

Matrix Matrix::columnAsMatrix(std::size_t column) const
{
    Q_ASSERT(column < columnCount());
    Matrix newMatrix(d->m_rows, 1);
    for (std::size_t i = 0; i < d->m_rows; ++i){
        newMatrix.setValue(i, 0, value(i, column));
    }
    return newMatrix;
}

Matrix Matrix::transposed() const
{
    Matrix newMatrix(columnCount(), rowCount());
    for(std::size_t i = 0; i < rowCount(); ++i) {
        for(std::size_t j = 0; j < columnCount(); ++j) {
            newMatrix.setValue(j, i, value(i, j));
        }
    }
    return newMatrix;
}

void Matrix::clearAndResize(std::size_t rows, std::size_t columns)
{
    if(d->m_data) {
        for(std::size_t i=0;i<d->m_rows;++i) {
            delete[] d->m_data[i];
        }
        delete[] d->m_data;
    }
    d-> m_data = new int*[rows];
    d->m_rows = rows;
    d->m_columns = columns;
    for (std::size_t i = 0; i < rows; ++i){
        d->m_data[i] = new int[columns];
        for(std::size_t j=0; j< columns;++j) {
            d->m_data[i][j] = 0;
        }
    }

}

Matrix Matrix::operator+(const Matrix &other)
{
    Q_ASSERT(rowCount() == other.rowCount());
    Q_ASSERT(columnCount() == other.columnCount());

    Matrix newMatrix(rowCount(), columnCount());
    for(std::size_t i = 0; i < rowCount(); ++i) {
        for(std::size_t j = 0; j < columnCount(); ++j) {
            newMatrix.setValue(i, j, value(i, j)+other.value(i, j));
        }
    }
    return newMatrix;
}

Matrix Matrix::operator-(const Matrix &other)
{
    Q_ASSERT(rowCount() == other.rowCount());
    Q_ASSERT(columnCount() == other.columnCount());

    Matrix newMatrix(rowCount(), columnCount());
    for(std::size_t i = 0; i < rowCount(); ++i){
        for(std::size_t j = 0; j < columnCount(); ++j){
            newMatrix.setValue(i, j, value(i, j)-other.value(i, j));
        }
    }
    return newMatrix;

}

Matrix Matrix::operator*(const Matrix &other)
{
    Q_ASSERT(columnCount() == other.rowCount());

    Matrix newMatrix(rowCount(), other.columnCount());
    for(std::size_t row = 0; row < rowCount(); ++row) {
        for(std::size_t column = 0; column < other.columnCount(); ++column) {
            int sum = 0;
            for(std::size_t i = 0; i < columnCount(); ++i) {
                sum += value(row,i)*other.value(i,column);
            }
            newMatrix.setValue(row, column, sum);
        }
    }
    return newMatrix;

}

int dotProduct(Matrix mat1, Matrix mat2)
{
    Q_ASSERT(mat1.columnCount() == mat2.rowCount());

    int sum(0);
    for(std::size_t i = 0; i < mat1.columnCount(); ++i){
        sum += mat1.value(0,i)*mat2.value(i,0);
    }
    return sum;
}

MatrixData::MatrixData() :
    QSharedData(),
    m_data(0),
    m_columns(0),
    m_rows(0)
{
}

MatrixData::MatrixData(const MatrixData &other) :
    QSharedData(other),
    m_data(new int*[other.m_rows]),
    m_columns(other.m_columns),
    m_rows(other.m_rows)
{
    for(std::size_t i=0;i<m_rows;++i) {
        m_data[i] = new int[m_columns];
        for(std::size_t j=0;j<m_columns;++j) {
            m_data[i][j] = other.m_data[i][j];
        }
    }
}

MatrixData::~MatrixData()
{
    if(m_data) {
        for(std::size_t i=0;i<m_rows;++i) {
            delete[] m_data[i];
            m_data[i] = 0;
        }
        delete[] m_data;
    }
}
