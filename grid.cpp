#include <QApplication>
#include <QGridLayout>
#include <QPushButton>
#include "gridcell.h"
#include "grid.h"

Grid::Grid(QWidget *parent) :
    QFrame(parent),
    m_size(0),
    m_showGrid(true),
    m_layout(new QGridLayout(this))
{
    m_layout->setContentsMargins(0,0,0,0);
    m_layout->setSpacing(0);
    setFrameShape(QFrame::Box);
    setLineWidth(1);
}

int Grid::size() const
{
    return m_size;
}

QList<int> Grid::toVector() const
{
    QList<int> ret;
    foreach( GridCell * cell, m_cells ) {
        ret.push_back(cell->isChecked()?1:-1);
    }
    return ret;
}

void Grid::setSize(int size)
{
    if(m_size != size) {
        emit sizeAboutToChange( size );
        m_size = size;
        foreach( GridCell * cell, m_cells ) {
            delete cell;
            cell = 0;
        }
        m_cells.clear();
        int row = 0;
        int col = 0;
        for( int i=0; i < (size*size); ++i ) {
            GridCell * cell = new GridCell(this);
            cell->setGridVisible(m_showGrid
                                 );
            m_cells.append(cell);
            if(col >= size) {
                col = 0;
                ++row;
            }
            m_layout->addWidget(cell, row, col++);
        }
        emit sizeChanged( m_size );
    }
    else {
        clear();
    }
}

void Grid::clear()
{
    foreach( GridCell * cell, m_cells ) {
        cell->setChecked(false);
    }
}

void Grid::fromVector(const QList<int> & input)
{
    Q_ASSERT(input.size() == static_cast<std::size_t>(m_cells.count()));
    for(int i=0;i<m_cells.count();++i) {
        if(input.at(i)>0)
            m_cells.at(i)->setChecked(true);
        else
            m_cells.at(i)->setChecked(false);
    }
}

void Grid::fromPredefined(int item)
{
    QList<int> data;
    switch(item) {
        case A8:
            data << -1 << -1 << -1 << 1 << 1 << -1 << -1 << -1;
            data << -1 << -1 << 1 << -1 << -1 << 1 << -1 << -1;
            data << -1 << 1 << -1 << -1 << -1 << -1 << 1 << -1;
            data << -1 << 1 << -1 << -1 << -1 << -1 << 1 << -1;
            data << -1 << 1 << 1 << 1 << 1 << 1 << 1 << -1;
            data << -1 << 1 << -1 << -1 << -1 << -1 << 1 << -1;
            data << -1 << 1 << -1 << -1 << -1 << -1 << 1 << -1;
            data << -1 << 1 << -1 << -1 << -1 << -1 << 1 << -1;
            break;
        case F8:
            data << -1 << 1 << 1 << 1 << 1 << 1 << 1 << -1;
            data << -1 << 1 << -1 << -1 << -1 << -1 << -1 << -1;
            data << -1 << 1 << -1 << -1 << -1 << -1 << -1 << -1;
            data << -1 << 1 << 1 << 1 << 1 << -1 << -1 << -1;
            data << -1 << 1 << -1 << -1 << -1 << -1 << -1 << -1;
            data << -1 << 1 << -1 << -1 << -1 << -1 << -1 << -1;
            data << -1 << 1 << -1 << -1 << -1 << -1 << -1 << -1;
            data << -1 << 1 << -1 << -1 << -1 << -1 << -1 << -1;
            break;
        case O8:
            data << -1 << -1 << -1 << 1 << 1 << -1 << -1 << -1;
            data << -1 << -1 << 1 << -1 << -1 << 1 << -1 << -1;
            data << -1 << 1 << -1 << -1 << -1 << -1 << 1 << -1;
            data << -1 << 1 << -1 << -1 << -1 << -1 << 1 << -1;
            data << -1 << 1 << -1 << -1 << -1 << -1 << 1 << -1;
            data << -1 << 1 << -1 << -1 << -1 << -1 << 1 << -1;
            data << -1 << -1 << 1 << -1 << -1 << 1 << -1 << -1;
            data << -1 << -1 << -1 << 1 << 1 << -1 << -1 << -1;
            break;
        case Equals8:
            data << -1 << -1 << -1 << -1 << -1 << -1 << -1 << -1;
            data << -1 << -1 << -1 << -1 << -1 << -1 << -1 << -1;
            data << -1 << 1 << 1 << 1 << 1 << 1 << 1 << -1;
            data << -1 << -1 << -1 << -1 << -1 << -1 << -1 << -1;
            data << -1 << -1 << -1 << -1 << -1 << -1 << -1 << -1;
            data << -1 << 1 << 1 << 1 << 1 << 1 << 1 << -1;
            data << -1 << -1 << -1 << -1 << -1 << -1 << -1 << -1;
            data << -1 << -1 << -1 << -1 << -1 << -1 << -1 << -1;
            break;
       case Box8:
            data << 1 << 1 << 1 << 1 << 1 << 1 << 1 << 1;
            data << 1 << -1 << -1 << -1 << -1 << -1 << -1 << 1;
            data << 1 << -1 << -1 << -1 << -1 << -1 << -1 << 1;
            data << 1 << -1 << -1 << -1 << -1 << -1 << -1 << 1;
            data << 1 << -1 << -1 << -1 << -1 << -1 << -1 << 1;
            data << 1 << -1 << -1 << -1 << -1 << -1 << -1 << 1;
            data << 1 << -1 << -1 << -1 << -1 << -1 << -1 << 1;
            data << 1 << 1 << 1 << 1 << 1 << 1 << 1 << 1;
            break;
       case Checker8:
            data << -1 << 1 << -1 << 1 << -1 << 1 << -1 << 1;
            data << 1 << -1 << 1 << -1 << 1 << -1 << 1 << -1;
            data << -1 << 1 << -1 << 1 << -1 << 1 << -1 << 1;
            data << 1 << -1 << 1 << -1 << 1 << -1 << 1 << -1;
            data << -1 << 1 << -1 << 1 << -1 << 1 << -1 << 1;
            data << 1 << -1 << 1 << -1 << 1 << -1 << 1 << -1;
            data << -1 << 1 << -1 << 1 << -1 << 1 << -1 << 1;
            data << 1 << -1 << 1 << -1 << 1 << -1 << 1 << -1;
            break;
    }
    fromVector(data);
}

void Grid::showGrid(bool show)
{
    m_showGrid = show;
    foreach( GridCell * cell, m_cells ) {
        cell->setGridVisible(show);
    }
}
