#ifndef GRIDCELL_H
#define GRIDCELL_H

#include <QAbstractButton>

class GridCell : public QAbstractButton
{
    Q_OBJECT
public:
    explicit GridCell(QWidget *parent = 0);
    QSize minimumSizeHint() const;
    QSize sizeHint() const;

public slots:
    void setGridVisible( bool visible );

protected:
    void paintEvent( QPaintEvent * );
    void enterEvent(QEvent * );

private:
    bool m_gridVisible;
};

#endif // GRIDCELL_H
