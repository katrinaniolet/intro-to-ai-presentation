#ifndef MATRIXDATADIALOG_H
#define MATRIXDATADIALOG_H

#include <QDialog>
class HopfieldNetwork;

namespace Ui {
class MatrixDataDialog;
}

class MatrixDataDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MatrixDataDialog(HopfieldNetwork * network, QWidget *parent = 0);
    ~MatrixDataDialog();

public slots:
    void resizeColumns();

private:
    Ui::MatrixDataDialog *ui;
};

#endif // MATRIXDATADIALOG_H
