#include <QtGui/QPainter>
#include <QtGui/QMouseEvent>
#include <QApplication>
#include <QDebug>
#include "gridcell.h"

GridCell::GridCell(QWidget *parent) :
    QAbstractButton(parent),
    m_gridVisible(true)
{
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    setCheckable(true);
}

QSize GridCell::minimumSizeHint() const
{
    return QSize(1,1);
}

QSize GridCell::sizeHint() const
{
    return QSize(9,9);
}

void GridCell::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    QColor fgColor = p.pen().color();
    if(isChecked()) {
        p.setBrush(fgColor);
    }
    if(isChecked() || m_gridVisible)
        p.drawRect(rect().adjusted(0,0,-1,-1));
}

void GridCell::enterEvent(QEvent *)
{
    if(QApplication::mouseButtons() & Qt::LeftButton) {
        toggle();
    }
    else
        qDebug() << QApplication::mouseButtons();
}

void GridCell::setGridVisible(bool visible)
{
    m_gridVisible = visible;
    update();
}
