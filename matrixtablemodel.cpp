#include "matrix.h"
#include "hopfieldnetwork.h"
#include "matrixtablemodel.h"

MatrixTableModel::MatrixTableModel(HopfieldNetwork *network, QObject *parent) :
    QAbstractTableModel(parent),
    m_network(network)
{
    connect(m_network,SIGNAL(dataAboutToChange()),this,SIGNAL(layoutAboutToBeChanged()));
    connect(m_network,SIGNAL(dataChanged()),this,SIGNAL(layoutChanged()));
}

int MatrixTableModel::rowCount(const QModelIndex &) const
{
    Q_ASSERT(m_network);
    return m_network->matrix().rowCount();
}

int MatrixTableModel::columnCount(const QModelIndex &) const
{
    Q_ASSERT(m_network);
    return m_network->matrix().columnCount();
}

QVariant MatrixTableModel::data(const QModelIndex &index, int role) const
{
    Q_ASSERT(m_network);
    if(role == Qt::DisplayRole) {
        return m_network->matrix().value(index.row(), index.column());
    }
    return QVariant();
}
