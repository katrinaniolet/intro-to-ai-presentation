#ifndef MATRIXTABLEMODEL_H
#define MATRIXTABLEMODEL_H

#include <QAbstractTableModel>
#include <QtCore/QVariant>
class HopfieldNetwork;

class MatrixTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit MatrixTableModel(HopfieldNetwork * network, QObject *parent = 0);

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;

signals:

public slots:

private:
    HopfieldNetwork * m_network;

};

#endif // MATRIXTABLEMODEL_H
