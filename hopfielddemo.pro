QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TARGET = hopfielddemo
TEMPLATE = app
HEADERS += mainwindow.h \
    gridcell.h \
    grid.h \
    matrix.h \
    hopfieldnetwork.h \
    matrixtablemodel.h \
    matrixdatadialog.h
SOURCES += mainwindow.cpp \
    main.cpp \
    gridcell.cpp \
    grid.cpp \
    matrix.cpp \
    hopfieldnetwork.cpp \
    matrixtablemodel.cpp \
    matrixdatadialog.cpp
FORMS += mainwindow.ui \
    matrixdatadialog.ui

